# hpod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://bitbucket.org/kxx355/learning-haskell)

This project contains a gitpod Docker file that installs the haskell platform

Use Haskell on the command line

```
stack repl
:load main.hs
```

ref:

https://www.haskell.org/onlinereport/standard-prelude.html
